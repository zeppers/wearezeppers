const app = require("express")();
const Sequelize = require("sequelize");
const bodyParser = require("body-parser");
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Acces-Control-Allow-Methods", "GET,POST,DELETE,PUT");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

const sequelize = new Sequelize("gamesapp", "masterYoda", "R2D2isHuman", {
  dialect: "mysql",
  define: {
    timestamps: false
  }
});
sequelize.authenticate();

const User = sequelize.define("user", {
  email: { type: Sequelize.STRING, primaryKey: true },
  password: Sequelize.STRING,
  nickname: Sequelize.STRING
});

const Image = sequelize.define("image", {
  path: { type: Sequelize.STRING, primaryKey: true },
  isCover: Sequelize.BOOLEAN
});

const Event = sequelize.define("event", {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  name: Sequelize.STRING,
  description: Sequelize.STRING,
  location: Sequelize.STRING,
  date: Sequelize.STRING
});

const Resource = sequelize.define("resource", {
  path: Sequelize.STRING
});

User.hasMany(Event);

const Game = sequelize.define("game", {
  id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
  name: Sequelize.STRING,
  description: Sequelize.STRING,
  genre: Sequelize.STRING,
  platforms: Sequelize.STRING,
  realease: Sequelize.INTEGER,
  rating: Sequelize.FLOAT
});

const Vote = sequelize.define("vote", {
  mark: Sequelize.INTEGER
});
const Atendee = sequelize.define("atendee", {});

Game.hasMany(Image);
Game.hasMany(Event);
Game.hasMany(Image);

Game.belongsToMany(User, { through: Vote });
User.belongsToMany(Game, { through: Vote });

User.belongsToMany(Event, { through: Atendee });
Event.belongsToMany(User, { through: Atendee });

sequelize.sync();
//home.html, game_detail.html, sign_in.html, all_games_list.html, create_event.html, register.html, home_signed.html
app.get("/app/home", async (req, res) => {
  res.sendFile("D:/codemyass/TW/Proiect/frontend.1/home.html");
});
// app.get("/app/css/home", async (req, res) => {
//   res.sendFile("D:/codemyass/TW/Proiect/frontend.1/style_home.css");
// });
app.get("/app/game_detail.html", async (req, res) => {
  res.sendFile("D:/codemyass/TW/Proiect/frontend.1/game_detail.html");
});
app.get("/app/all_games_list.html", async (req, res) => {
  res.sendFile("D:/codemyass/TW/Proiect/frontend.1/all_games_list.html");
});
app.get("/app/create_event.html", async (req, res) => {
  res.sendFile("D:/codemyass/TW/Proiect/frontend.1/create_event.html");
});
app.get("/app/register.html", async (req, res) => {
  res.sendFile("D:/codemyass/TW/Proiect/frontend.1/register.html");
});
// app.get("/app/home_signed.html", async (req, res) => {
//   res.sendFile("D:/codemyass/TW/Proiect/frontend.1/home_signed.html");
// });
app.get("/app/sign_in.html", async (req, res) => {
  res.sendFile("D:/codemyass/TW/Proiect/frontend.1/sign_in.html");
});

app.get("/images/:id", async (req, res) => {
  try {
    let image = await Image.findOne({
      where: { path: req.params.id },
      raw: true
    });
    if (image) {
      res.sendfile("./images/" + req.params.id);
    } else {
      res.status(404).send(JSON.stringify({ message: "not found!" }));
    }
  } catch (e) {
    console.warn(e);
    res.status(500).send(JSON.stringify({ message: "server error!" }));
  }
});

app.post("/login", async (req, res) => {
  try {
    let credentials = req.body.credentials;
    let user = await User.findOne({
      where: { email: req.body.email, password: req.body.password },
      raw: true
    });
    console.log(user);
    if (user) {
      res.status(201).send(JSON.stringify(user));
    } else {
      res.status(401).send(JSON.stringify({ message: "Unauthorized!" }));
    }
  } catch (e) {
    console.warn(e);
    res.status(500).send(JSON.stringify({ message: "server error!" }));
  }
});

app.post("/register", async (req, res) => {
  try {
    let user = req.body;
    try {
      await User.create(user);
      res.status(201).send(JSON.stringify({ message: "created!" }));
    } catch (e) {
      res.status(400).send(JSON.stringify({ message: "failed!" }));
    }
  } catch (e) {
    console.warn(e);
    res.status(500).send(JSON.stringify({ message: "server error!" }));
  }
});

app.get("/resources/:id", async (req, res) => {
  try {
    res.sendfile("D:/codemyass/TW/Proiect/frontend.1/res/" + req.params.id);
  } catch (e) {
    console.warn(e);
    res.status(500).send(JSON.stringify({ message: "server error!" }));
  }
});

app.post("/events", async (req, res) => {
  let credentials = req.body.credentials;

  try {
    await Event.create(req.body);
    res.status(201).send(JSON.stringify({ message: "created!" }));
  } catch (e) {
    res.status(400).send(JSON.stringify({ message: "failed!" }));
  }
});

app.post("/vote", async (req, res) => {
  try {
    let credentials = req.body.credentials;
    let user = await User.findOne({
      where: { email: credentials.email, password: credentials.password },
      raw: true
    });
    if (user) {
      let vote = req.body.vote;
      let voteDB = await Vote.findAll({
        where: { gameId: vote.gameId, userEmail: vote.userEmail }
      });
      if (voteDB.length == 0) {
        await Vote.create(vote);
        let votesForThisGame = await Vote.findAll({
          where: { gameId: vote.gameId },
          raw: true
        });
        let noVotes = votesForThisGame.length;
        noVotes--;
        let game = await Game.findByPrimary(vote.gameId);
        let newRating = (game.rating * noVotes + vote.mark) / (noVotes + 1);
        // console.log(gameCopy);
        await game.update({ rating: newRating });
        res.status(202).send(JSON.stringify({ message: "accepted!" }));
      } else res.status(400).send(JSON.stringify({ message: "failed!" }));
    }
  } catch (e) {
    console.warn(e);
    res.status(500).send(JSON.stringify({ message: "server error!" }));
  }
});

app.post("/atendees", async (req, res) => {
  try {
    let credentials = req.body.credentials;
    let user = await User.findOne({
      where: { email: credentials.email, password: credentials.password },
      raw: true
    });
    if (user) {
      let atendee = req.body.atendee;
      let atendeeDB = await Atendee.findAll({
        where: { userEmail: atendee.userEmail, eventId: atendee.eventId }
      });
      if (atendeeDB.length == 0) {
        await Atendee.create(atendee);
        res.status(201).send(JSON.stringify({ message: "created" }));
      } else {
        res.status(400).send(JSON.stringify({ message: "failed!" }));
      }
    }
  } catch (e) {
    console.warn(e);
    res.status(500).send(JSON.stringify({ message: "server error!" }));
  }
});

app.get("/games/:id", async (req, res) => {
  try {
    let gameDB = await Game.findOne({
      where: { id: req.params.id },
      raw: true
    });
    if (gameDB) {
      let game = gameDB;
      let images = await Image.findAll({
        where: { gameId: gameDB.id },
        raw: true
      });
      if (images) game.images = images;
      let events = await Event.findAll({
        where: { gameId: game.id },
        raw: true
      });
      if (events) {
        events.forEach(evt => {
          delete evt.gameId;
          game.events = events;
        });
      }
      res.status(200).send(JSON.stringify(game));
    } else {
      res.status(404).send(JSON.stringify({ message: "not found!" }));
    }
  } catch (e) {
    console.warn(e);
    res.status(500).send(JSON.stringify({ message: "server error!" }));
  }
});

app.get("/games", async (req, res) => {
  try {
    let games = await Game.findAll({
      where: req.params.query,
      raw: true
    });
    if (games) {
      for (let i = 0; i < games.length; i++) {
        let cover = await Image.findOne({
          where: {
            gameId: games[i].id,
            isCover: true
          },
          raw: true
        });
        if (cover) games[i].cover = cover;
      }
      res.status(200).send(JSON.stringify(games));
    }
  } catch (e) {
    console.warn(e);
    res.status(500).send(JSON.stringify({ message: "server error!" }));
  }
});
app.get("/bestGames/:nr", async (req, res) => {
  try {
    let games = await Game.findAll({
      order: [["rating", "DESC"]],
      limit: parseInt(req.params.nr),
      raw: true
    });
    if (games) {
      for (let i = 0; i < games.length; i++) {
        let cover = await Image.findOne({
          where: {
            gameId: games[i].id,
            isCover: true
          },
          raw: true
        });
        if (cover) games[i].cover = cover;
      }
      res.status(200).send(JSON.stringify(games));
    }
  } catch (e) {
    console.warn(e);
    res.status(500).send(JSON.stringify({ message: "server error!" }));
  }
});

// app.get("/events/:id",async (req,res)=>{
//     try{
//         let event = Event.findOne({where:{id:req.params.id},raw:true});
//         if(event){

//         }
//         else{
//             res.status(500).send(JSON.stringify({message:'server error!'}));
//         }
//     }
//     catch(e){
//         console.warn(e);
//         res.status(500).send(JSON.stringify({ message: "server error!" }));
//     }
// });

app.listen(3000, () => {
  console.log("listening on port 3000");
});
