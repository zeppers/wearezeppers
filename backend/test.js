const app = require('express')();
const mysql = require('mysql');
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Acces-Control-Allow-Methods','GET,POST,DELETE,PUT');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

var con = mysql.createConnection({
    host:'127.0.0.1',
    user:'root',
    password:'',
    database:'gamesApp'
});

con.connect();

app.post('/vote',(req,res)=>{
    let user = req.body.user;
    let game_id = req.body.game_id;
    let vote = req.body.vote;
    con.query("select * from users where email='"+user.email+"' and password='"+user.password+"'",(err,rows)=>{
        if(rows.length!=0){
            con.query('select * from votes where game_id='+game_id,(err,rows)=>{
                let bigString = rows[0].users;
                let usersThatVoted=bigString.split(",");
                if(usersThatVoted.includes(user.id.toString()))
                    res.status(400).send(JSON.stringify({'message':'you already voted for this game!'}));
                else{
                    bigString=bigString+","+user.id.toString();
                    con.query("update votes set users='"+bigString+"' where game_id="+game_id,(err,rows)=>{
                        res.status(200).send(JSON.stringify({'message':'accepted!'}));
                        con.query("select * from games where id="+game_id,(err,rows)=>{
                            let game = rows[0];
                            let newRating=(game.rating*game.rating_count+vote)/(game.rating_count+1);
                            con.query("update games set rating="+newRating+", rating_count="+(game.rating_count+1)+" where id="+game.id);
                        })
                    })
                }
            })
        }
        else
            res.status(403).send(JSON.stringify({'message':'you have no authorization!'}));
    })
})



//Covers route
app.get('/covers',(req,res)=>{
    con.query('select * from covers',(err,rows)=>{
        res.status(200).send(JSON.stringify(rows));
    })
})
app.get('/covers/:id',(req,res)=>{
    con.query('select * from covers where id='+req.params.id,(err,rows)=>{
        if(rows[0]){
            res.status(200).sendFile(__dirname+'/covers/'+rows[0].name+'.png');
        }
        else
            res.status(404).send(JSON.stringify({'message':'resource not found!'}));
    });
});
/////////
//Locations route
app.get('/locations',(req,res)=>{
    con.query('select * from locations',(err,rows)=>{
        res.status(200).send(JSON.stringify(rows[0]));
    })
})
app.get('/locations/:id',(req,res)=>{
    con.query('select * from locations where id='+req.params.id,(err,rows)=>{
        let location = rows[0];
        if(location){
            res.status(200).send(JSON.stringify(location));
        }
        else
            res.status(404).send(JSON.stringify({'message':'resource not found!'}));
    })
})
///
//GAMES ROUTE
let bestGames = [];
app.get('/games',(req,res)=>{
    con.query('select * from covers',(err,rows)=>{
        let covers = rows;
        con.query('select * from games',(err,rows)=>{
            let games = rows;
            AssignCoversToGames(games,covers);
            res.status(200).send(JSON.stringify(games));
        })
    })
});

app.get('/games/:id',(req,res)=>{
    con.query('select * from games where id='+req.params.id,(err,rows)=>{
        let game = rows[0];
        if(game){
            con.query('select * from covers where id='+game.cover_id,(err,rows)=>{
                game.cover = rows[0];
                res.status(200).send(JSON.stringify(game));
            })
        }
        else
            res.status(404).send(JSON.stringify({'message':'resource not found!'}));
    })
})


app.get('/games/:id/cover',(req,res)=>{
    con.query('select * from games where id='+req.params.id,(err,rows)=>{
        let game = rows[0];
        if(game){
            con.query('select * from covers where id='+game.cover_id,(err,rows)=>{
                res.status(200).sendFile(__dirname+'/covers/'+rows[0].name+'.png');
            })
        }
        else
            res.status(404).send(JSON.stringify({'message':'resource not found!'}));
    })
})
app.get('/games/:id/events',(req,res)=>{
    con.query('select * from games where id='+req.params.id,(err,rows)=>{
        let game = rows[0];
        if(game){
            con.query('select * from covers where id='+game.cover_id,(err,rows)=>{
                delete game.cover_id;
                game.cover = rows[0];
                con.query('select * from events where game_id='+game.id,(err,rows)=>{
                    let events = rows;
                    con.query('select * from locations',(err,rows)=>{
                        AssignLocationsToEvents(events,rows);
                        events.forEach(event=>{
                            delete event.game_id;
                            event.game = game;
                        })
                        res.status(200).send(JSON.stringify(events));
                    })
                })
            })
        }
        else
            res.status(404).send(JSON.stringify({'message':'resource not found!'}));
    })
})
app.get('/bestGames/:id',(req,res)=>{
    con.query('select * from covers',(err,rows)=>{
        let covers = rows;
        con.query('select * from games',(err,rows)=>{
            let games = rows;
            AssignCoversToGames(games,covers);
            SortGamesByRating(games);
            while(games.length>req.params.id)
                games.pop();
            res.status(200).send(JSON.stringify(games));
        })
    })
})

//////
//Events route
app.get('/events',(req,res)=>{
    con.query('select * from covers',(err,rows)=>{
        let covers = rows;
        con.query('select * from games',(err,rows)=>{
            let games = rows;
            AssignCoversToGames(games,covers);
            con.query('select * from locations',(err,rows)=>{
                let locations = rows;
                con.query('select * from events',(err,rows)=>{
                    let events = rows;
                    AssignGamesToEvents(events,games);
                    AssignLocationsToEvents(events,locations);
                    res.status(200).send(JSON.stringify(events));
                })
            })
        })
    })
})
app.get('/events/:id',(req,res)=>{
    con.query('select * from events where id='+req.params.id,(err,rows)=>{
        let event = rows[0];
        if(event){
            con.query('select * from locations where id='+event.location_id,(err,rows)=>{
                delete event.location_id;
                event.location = rows[0];
                con.query('select * from games where id='+event.game_id,(err,rows)=>{
                    let game = rows[0];
                    delete event.game_id;
                    con.query('select * from covers where id='+game.cover_id,(err,rows)=>{
                        delete game.cover_id;
                        game.cover = rows[0];
                        event.game = game;
                        res.status(200).send(JSON.stringify(event));
                    })
                })
            })
        }
        else
            res.status(404).send(JSON.stringify({'message':'resource not found!'}));
    })
})
app.get('/events/:id/location',(req,res)=>{
    con.query('select * from events where id='+req.params.id,(err,rows)=>{
        let event = rows[0];
        if(event){
            con.query('select * from locations where id='+event.location_id,(err,rows)=>{
                res.status(200).send(JSON.stringify(rows[0]));
            })
        }
        else
            res.status(404).send(JSON.stringify({'message':'resource not found!'}));
    })
})
app.get('/events/:id/game',(req,res)=>{
    con.query('select * from events where id='+req.params.id,(err,rows)=>{
        let event = rows[0];
        if(event){
            con.query('select * from games where id='+event.game_id,(err,rows)=>{
                let game = rows[0];
                con.query('select * from covers where id='+game.cover_id,(err,rows)=>{
                    delete game.cover_id;
                    game.cover = rows[0];
                    res.status(200).send(JSON.stringify(game));
                })
            })
        }
        else
            res.status(404).send(JSON.stringify({'message':'resource not found!'}));
    })
})

app.post('/events',(req,res)=>{
    let admin = req.body.admin;
    if(admin){
        con.query("select * from admins where email='"+admin.email+"' and password='"+admin.password+"'",(err,rows)=>{
            if(rows.length!=0){
                let event = req.body.event;
                if(event){
                    con.query("insert into events values(NULL,'"+event.name+"','"+event.description+"',"+event.location_id+",'"+event.date+"','"+event.game_id+"',0)");
                    res.status(201).send(JSON.stringify({message:'created!'}));
                }
                else
                    res.status(400).send(JSON.stringify({message:'failed!'}));
            }
            else
                res.status(403).send(JSON.stringify({message:'you have no authorization!'}));
        })
    }
    else
        res.status(400).send(JSON.stringify({message:'failed!'}));
})

app.delete('/events/:id',(req,res)=>{
    let admin = req.body.admin;
    if(admin){
        con.query("select * from admins where email='"+admin.email+"' and password='"+admin.password+"'",(err,rows)=>{
            if(rows.length!=0){
                con.query("delete from events where id="+req.params.id,(err,rows)=>{
                    if(rows.affectedRows!=0)
                        res.status(202).send(JSON.stringify({message:'accepted!'}));
                    else
                        res.status(404).send(JSON.stringify({message:'not found'}));
                });
            }
            else
                res.status(403).send(JSON.stringify({message:'you have no authorization!'}));
        })
    }
    else
        res.status(400).send(JSON.stringify({message:'failed!'})); 
})

app.post('/adminlogin',(req,res)=>{
    let admin = req.body.admin;
    if(admin){
        con.query("select * from admins where email='"+admin.email+"' and password='"+admin.password+"'",(err,rows)=>{
            if(rows.length!=0){
                res.status(200).send(JSON.stringify(rows[0]));
            }
            else
                res.status(404).send(JSON.stringify({'message':'not found!'}));
        })
    }
})

app.post('/login',(req,res)=>{
    let user = req.body.user;
    if(user){
        con.query("select * from users where email='"+user.email+"' and password='"+user.password+"'",(err,rows)=>{
            if(rows.length!=0){
                res.status(200).send(JSON.stringify(rows[0]))
            }
            else
                res.status(404).send(JSON.stringify({'message':'not found!'}));
        })
    }
})
app.post('/users',(req,res)=>{
    let user = req.body.user;
    if(user){
        con.query("select * from users where email='"+user.email+"'",(err,rows)=>{
            if(rows.length!=0)
                res.status(400).send(JSON.stringify({'message':'failed!'}));
            else{
                con.query("insert into users values('"+user.email+"','"+user.password+"','"+user.nickname+"')");
                res.status(201).send(JSON.stringify({'message':'created!'}));
            }
        })
    }
})

function AssignCoversToGames(games,covers){
    for(let i = 0;i<games.length;i++)
        for(let j = 0;j<covers.length;j++)
            if(games[i].cover_id==covers[j].id){
                games[i].cover = covers[j];
                delete games[i].cover_id;
            }
}
function AssignGamesToEvents(events,games){
    for(let i = 0;i<events.length;i++)
        for(let j = 0;j<games.length;j++)
            if(events[i].game_id==games[j].id){
                events[i].game = games[j];
                delete events[i].game_id;
            }
}
function AssignLocationsToEvents(events,locations){
    for(let i = 0;i<events.length;i++)
        for(let j =0;j<locations.length;j++)
            if(events[i].location_id==locations[j].id){
                events[i].location=locations[j];
                delete events[i].location_id;
            }
}
function SortGamesByRating(games){
    let ratings = [];
    games.forEach(game=>{
        ratings.push(game.rating);
        ratings.sort();
        ratings.reverse();
    })
    for(let i = 0;i<games.length;i++)
            games[i].rating = ratings[i];
}

app.listen(8000,()=>{
    console.log('server started on port 8000...');
})
